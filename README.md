# Concrete5CopyRight
[![Concrete5CopyRight](https://radeff.red/pics/git/c5copyright2.png)](Concrete5CopyRight)

## Français
Un block concrete5 pour afficher une information de copyright sur votre site

# Instructions pour l'installation
- Installer le package sous votre répertoire /packages
- Chercher dans votre interface d'administration "Améliorer concrete5"
- Chercher le packages _"Concrete5CopyRight"_
- Installer le package

Vous pouvez maintenant:
- inclure le bloc _CopyRight_ où vous le souhaitez sur votre site

---

## English:
A package to include copyright informations on your concrete5's website

# Installation Instructions
- Install package in your site's /packages directory
- Go to "Extend concrete5 > Add Functionality"
- Activate package _"Concrete5CopyRight"_

You may now:
- include the block _CopyRight_ anywhere on your concrete5 website
