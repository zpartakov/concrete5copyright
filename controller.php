<?php
/**
 *
 * The controller for the concrete5copyright package.
 *
 * @package    concrete5copyright
 *
 * @author     Fred Radeff <fradeff@akademia.ch>
 * @copyright  Copyright (c) 2020 radeff.red (https://radeff.red)
 * @license    http://www.gnu.org/licenses/ GNU General Public License
 */
namespace Concrete\Package\Concrete5copyright;
use Package;
use BlockType;
use View;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package {

	protected $pkgHandle = 'concrete5copyright';
	protected $appVersionRequired = '5.8.5.2';
	protected $pkgVersion = '0.0.1';


	public function getPackageName()
	{
		return t("Concrete5copyright");
	}

	public function getPackageDescription()
	{
		return t("Have a copyright block on your website");
	}

	public function install()
	{
		$pkg = parent::install();
		// install block
		BlockType::installBlockTypeFromPackage('concrete5copyright', $pkg);
		return $pkg;
	}

	public function uninstall() {
		parent::uninstall();
		$db = \Database::connection();
	}

}
