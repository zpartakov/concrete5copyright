<div id="c5copyright">
	<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
//display copyright
echo "&copy;";
echo "&nbsp;";
echo "Copyright";
//compute date+5 years
$dateDeb=date("Y");
$dateFin=$dateDeb+5;
echo "&nbsp;";
echo $dateDeb ."-" .$dateFin;
echo "&nbsp;";
//display site name
$site = Core::make('site')->getSite();
$config = $site->getConfigRepository();
echo $config->get('name');
?>
</div>
