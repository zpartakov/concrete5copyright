<?php
namespace Concrete\Package\Concrete5copyright\Block\Concrete5copyright;
use Package;
use View;
use Loader;
use Page;
use Core;
use \Concrete\Core\Block\BlockController;

class Controller extends BlockController{

	protected $btInterfaceWidth = "200";
	protected $btInterfaceHeight = "200";
	public function getBlockTypeDescription() {
		return t("This concrete5 block displays a dynamic copyright");
	}

	public function getBlockTypeName() {
		return t("Concrete5copyright");
	}

	public function view(){
	}
}

?>
